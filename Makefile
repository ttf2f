# Component settings
COMPONENT := ttf2f
COMPONENT_VERSION := 0.0.7
COMPONENT_DATE := 27 May 2022
# We produce an application binary
COMPONENT_TYPE := binary

# Default frontend is cli
FRONTEND ?= cli

# Setup the tooling
PREFIX ?= /opt/netsurf
NSSHARED ?= $(PREFIX)/share/netsurf-buildsystem
include $(NSSHARED)/makefiles/Makefile.tools

# Toolchain flags
WARNFLAGS := -Wall -Wextra -Wundef -Wpointer-arith -Wcast-align \
	-Wwrite-strings -Wstrict-prototypes -Wmissing-prototypes \
	-Wmissing-declarations -Wnested-externs -Werror #-pedantic
CFLAGS := $(CFLAGS) -std=c99 \
	-D_BSD_SOURCE -D_DEFAULT_SOURCE -D_POSIX_C_SOURCE \
	-I$(CURDIR)/include/ -I$(CURDIR)/src $(WARNFLAGS) -Dasm="__asm"

# Freetype2
ifneq ($(findstring clean,$(MAKECMDGOALS)),clean)
  ifneq ($(PKGCONFIG),)
    ifeq ($(findstring -riscos,$(HOST)),-riscos)
      CFLAGS := $(CFLAGS) $(shell $(PKGCONFIG) freetype2 --cflags --static)
      LDFLAGS := $(LDFLAGS) $(shell $(PKGCONFIG) freetype2 --libs --static)
    else
      CFLAGS := $(CFLAGS) $(shell $(PKGCONFIG) freetype2 --cflags)
      LDFLAGS := $(LDFLAGS) $(shell $(PKGCONFIG) freetype2 --libs)
    endif
  else
    CFLAGS := $(CFLAGS) -I$(PREFIX)/include/freetype2
    LDFLAGS := $(LDFLAGS) -lfreetype -lz
  endif
endif

# OSLib (RISC OS target only)
ifneq ($(findstring clean,$(MAKECMDGOALS)),clean)
  ifeq ($(findstring -riscos,$(HOST)),-riscos)
    CFLAGS := $(CFLAGS) -I$(PREFIX)/include -static
    LDFLAGS := $(LDFLAGS) -lOSLib32 -lOSLibSupport32 -static
  endif
endif

include $(NSBUILD)/Makefile.top

ifeq ($(findstring -riscos,$(HOST)),-riscos)
  realoutput := $(OUTPUT)
  e1f := ,e1f
  ifeq ($(EXEEXT),$(e1f))
    ff8 := ,ff8
    realoutput := $(subst $(EXEEXT),$(ff8),$(OUTPUT))

    POST_TARGETS := $(realoutput) $(POST_TARGETS)

    ELF2AIF_FLAGS :=
    ifeq ($(findstring -gnueabi,$(HOST)),-gnueabi)
      ELF2AIF_FLAGS := -e $(ELF2AIF_FLAGS)
    endif

$(realoutput): $(OUTPUT)
	$(Q)echo " ELF2AIF: $@"
	$(Q)$(dir $(CC))elf2aif $(ELF2AIF_FLAGS) $(OUTPUT) $@
	$(Q)echo " SQUEEZE: $@"
	$(Q)$(dir $(CC))squeeze $@

  endif

  DISTCLEAN_ITEMS := $(DISTCLEAN_ITEMS) ttf2f.zip

.PHONY: riscos-dist
riscos-dist: $(realoutput) $(BUILDDIR)/Res,fae $(BUILDDIR)/!Boot,feb $(BUILDDIR)/!Help
	$(Q)$(CP) $(CPFLAGS) $(realoutput) !TTF2f/!RunImage,ff8
	$(Q)$(CP) $(CPFLAGS) $(BUILDDIR)/Res,fae !TTF2f/Res,fae
	$(Q)$(CP) $(CPFLAGS) $(BUILDDIR)/!Help !TTF2f/!Help
	$(Q)$(CP) $(CPFLAGS) $(BUILDDIR)/!Boot,feb !TTF2f/!Boot,feb
	$(Q)$(dir $(CC))zip -9r, ttf2f.zip !TTF2f
	$(Q)$(RM) $(RMFLAGS) !TTF2f/!RunImage,ff8 !TTF2f/Res,fae !TTF2f/!Help !TTF2f/!Boot,feb

  __empty :=
  __space := $(empty) $(empty)
$(BUILDDIR)/Res,fae: riscos/Res
	$(Q)echo "   CCRES: $@"
	$(Q)$(SED) \
		-e 's/__COPYRIGHT_END__/$(word $(words $(COMPONENT_DATE)),$(COMPONENT_DATE))/' \
		-e 's/__DATE__/$(subst $(__space),-,$(COMPONENT_DATE))/' \
		-e 's/__VERSION__/$(word 1,$(subst ., ,$(COMPONENT_VERSION))).$(subst $(__space),,$(wordlist 2,$(words $(subst ., ,$(COMPONENT_VERSION))),$(subst ., ,$(COMPONENT_VERSION))))/' \
		$^ >$(BUILDDIR)/Res.txt
	$(Q)$(dir $(CC))ccres $(BUILDDIR)/Res.txt $@

$(BUILDDIR)/!Boot,feb: riscos/Boot
	$(Q)echo "     SED: $@"
	$(Q)$(SED) \
		-e 's/__DATE__/$(COMPONENT_DATE)/' \
		-e 's/__VERSION__/$(word 1,$(subst ., ,$(COMPONENT_VERSION))).$(subst $(__space),,$(wordlist 2,$(words $(subst ., ,$(COMPONENT_VERSION))),$(subst ., ,$(COMPONENT_VERSION))))/' \
		$^ >$@

$(BUILDDIR)/!Help: riscos/Help
	$(Q)echo "     SED: $@"
	$(Q)$(SED) \
		-e 's/__COPYRIGHT_END__/$(word $(words $(COMPONENT_DATE)),$(COMPONENT_DATE))/' \
		$^ >$@

endif

# Redefine the default target so our new POST_TARGETS take effect
all: $(PRE_TARGETS) $(OUTPUT) $(POST_TARGETS)

# Extra installation rules

